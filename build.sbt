name := "alpakka-kafka"
version := "0.1"
scalaVersion := "2.13.8"
fork := true

val akkaVersion = "2.6.18"
val akkaStreamKafkaVersion = "3.0.0"

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-stream-kafka" % akkaStreamKafkaVersion,
  "com.typesafe.akka" %% "akka-stream" % akkaVersion,
  "com.typesafe.akka" %% "akka-actor-typed" % akkaVersion,
  "ch.qos.logback" % "logback-classic" % "1.2.6",

  // testing
  "org.scalatest" %% "scalatest" % "3.2.10" % Test,
  "org.testcontainers" % "kafka" % "1.15.3" % Test,
  "com.typesafe.akka" %% "akka-actor-testkit-typed" % akkaVersion % Test,
  "com.typesafe.akka" %% "akka-stream-kafka-testkit" % "3.0.0" % Test,
  "com.typesafe.akka" %% "akka-stream-testkit" % akkaVersion % Test
)
