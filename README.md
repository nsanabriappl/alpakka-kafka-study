
## Alpakka Kafka

This playground project is intended for learning purposes. It explores some capabilities of the Alpakka Kafka library.

For more information, visit https://doc.akka.io/docs/alpakka-kafka/current/home.html.