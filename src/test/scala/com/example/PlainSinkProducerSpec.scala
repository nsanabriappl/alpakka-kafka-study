package com.example

import akka.Done
import akka.kafka.scaladsl.{ Consumer, Producer }
import akka.kafka.testkit.scaladsl.TestcontainersKafkaLike
import akka.kafka.{ ProducerSettings, Subscriptions }
import akka.stream.scaladsl.{ Keep, Sink, Source }
import akka.stream.testkit.scaladsl.TestSink
import org.apache.kafka.clients.producer.ProducerRecord
import org.apache.kafka.common.serialization.StringSerializer

import scala.concurrent.Future

class PlainSinkProducerSpec extends KafkaBaseSpec with TestcontainersKafkaLike {

  "Producer.plainSink" must {

    "publish messages to Kafka" in {
      val topic = createTopic(1)
      val group = createGroupId(1)

      val config = system.settings.config.getConfig("akka.kafka.producer")

      val producerSettings =
        ProducerSettings(config, new StringSerializer, new StringSerializer)
          .withBootstrapServers(bootstrapServers)

      val consumerSettings =
        consumerDefaults(StringDeserializer, StringDeserializer).withGroupId(group)

      //   Input                           Mat value
      val producer: Sink[ProducerRecord[String, String], Future[Done]] =
        Producer.plainSink(producerSettings)

      val sampleMessages = List("Messi", "Ronaldo", "Pele")

      val published: Future[Done] =
        Source(sampleMessages)
          .map(_ + "-published")
          .map(message => new ProducerRecord[String, String](topic, message))
          .runWith(producer)

      val probe = Consumer
        .plainSource(consumerSettings, Subscriptions.topics(topic))
        .map(_.value())
        .toMat(TestSink.probe)(Keep.right)
        .run()

      val consumed = probe
        .request(sampleMessages.size)
        .expectNextN(sampleMessages.size)

      consumed should contain theSameElementsInOrderAs sampleMessages.map(_ + "-published")

      probe.cancel()
      published.futureValue shouldBe Done
    }
  }

}
