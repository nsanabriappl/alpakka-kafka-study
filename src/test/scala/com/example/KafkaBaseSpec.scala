package com.example

import akka.kafka.testkit.scaladsl.ScalatestKafkaSpec
import org.scalatest.concurrent.{ Eventually, ScalaFutures }
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpecLike

abstract class KafkaBaseSpec(kafkaPort: Int)
    extends ScalatestKafkaSpec(kafkaPort)
       with AnyWordSpecLike
       with Matchers
       with ScalaFutures
       with Eventually {

  protected def this() = this(kafkaPort = -1)
}
