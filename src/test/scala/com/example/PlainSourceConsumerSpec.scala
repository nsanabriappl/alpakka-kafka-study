package com.example

import akka.kafka.Subscriptions
import akka.kafka.scaladsl.Consumer
import akka.kafka.testkit.scaladsl.TestcontainersKafkaLike
import akka.stream.scaladsl.Keep
import akka.stream.testkit.scaladsl.TestSink
import org.apache.kafka.clients.consumer.ConsumerRecord

import scala.concurrent.Await

class PlainSourceConsumerSpec extends KafkaBaseSpec with TestcontainersKafkaLike {

  "Consumer.plainSink" must {

    "consume messages" in {
      val inboundTopic = createTopic(1)
      val groupId = createGroupId(1)

      val sampleMessages = List("Messi", "Ronaldo", "Pele")

      Await.result(produceString(inboundTopic, sampleMessages), remainingOrDefault)

      val consumerSettings =
        consumerDefaults(StringDeserializer, StringDeserializer).withGroupId(groupId)

      val (control, probe) = Consumer
        .plainSource(consumerSettings, Subscriptions.topics(inboundTopic))
        .map { msg: ConsumerRecord[String, String] =>
          msg.value() + "-consumed"
        }
        .toMat(TestSink.probe)(Keep.both)
        .run()

      val consumed = probe
        .request(3)
        .expectNextN(3)

      consumed should contain theSameElementsInOrderAs sampleMessages.map(_ + "-consumed")

      probe.cancel()



      val (control2, probe2) = Consumer
        .plainSource(consumerSettings, Subscriptions.topics(inboundTopic))
        .map(_.value() + "-consumed")
        .toMat(TestSink.probe)(Keep.both)

        .run()


      val consumed2 = probe
        .request(3)
        .expectNoMessage()

      consumed shouldBe empty

    }
  }

}
