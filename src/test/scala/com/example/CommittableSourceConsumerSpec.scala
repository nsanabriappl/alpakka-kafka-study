package com.example

import akka.Done
import akka.kafka.ProducerMessage.Result
import akka.kafka.scaladsl.Consumer.DrainingControl
import akka.kafka.scaladsl.{Committer, Consumer, Producer}
import akka.kafka.testkit.scaladsl.TestcontainersKafkaLike
import akka.kafka.{CommitterSettings, ConsumerMessage, ProducerMessage, Subscriptions}
import akka.stream.scaladsl.{Keep, Sink}
import akka.stream.testkit.scaladsl.TestSink
import org.apache.kafka.clients.producer.ProducerRecord

import scala.concurrent.duration._
import scala.concurrent.{Await, Future}

class CommittableSourceConsumerSpec extends KafkaBaseSpec with TestcontainersKafkaLike {

  "Consumer.committableSource" must {

    "consume messages" in {
      val inboundTopic = createTopic(1)
      val groupId = createGroupId(1)

      val sampleMessages = List("Messi", "Ronaldo", "Pele")

      Await.result(produceString(inboundTopic, sampleMessages), remainingOrDefault)

      val consumerSettings =
        consumerDefaults(StringDeserializer, StringDeserializer).withGroupId(groupId)

      val committerSettings = CommitterSettings(system)

      val (control, probe) = Consumer
        .committableSource(consumerSettings, Subscriptions.topics(inboundTopic))
        .mapAsync(1) { msg: ConsumerMessage.CommittableMessage[String, String] =>
          businessOperation(msg.record.value())
            .map( _ => msg.committableOffset)
        }
        .toMat(Committer.sink(committerSettings))(Keep.both)
        .run()



      val (control2, probe2) = Consumer
        .plainSource(consumerSettings, Subscriptions.topics(inboundTopic))
        .map(_.value() + "-consumed")
        .toMat(TestSink.probe)(Keep.both)
        .run()


      val consumed2 = probe2
        .request(3)
        .expectNextN(0)

      consumed2 shouldBe empty

    }

    "consume and produce with Producer.flexiflow" in {
      val consumerSettings = consumerDefaults.withGroupId(createGroupId())

      val sampleElements = List("Messi", "Ronaldo", "Pele")

      val inboundTopic = createTopic(1)
      val outboundTopic = createTopic(3)

      val producerSettings = producerDefaults
      val committerSettings = committerDefaults

      val control =
        Consumer
          .committableSource(consumerSettings, Subscriptions.topics(inboundTopic))
          .map{ inboundMessage =>
              ProducerMessage.single(
                new ProducerRecord(outboundTopic, inboundMessage.record.key, inboundMessage.record.value + "-published"),
                inboundMessage.committableOffset
              )
          }
          .via(Producer.flexiFlow(producerSettings))
          .map { result: ProducerMessage.Results[String, String, ConsumerMessage.CommittableOffset] =>

            result match {
              case Result(metadata, message) =>
                 // println("key  => " + message.key())
                  println("partition => " + metadata.partition())
                  println("offset => " +  metadata.offset())
              case _ =>
            }

            result.passThrough
          }
          .toMat(Committer.sink(committerSettings))(DrainingControl.apply)
          .run()

      val (control2, result) = Consumer
        .plainSource(consumerSettings, Subscriptions.topics(outboundTopic))
        .toMat(Sink.seq)(Keep.both)
        .run()

      awaitProduce(produceString(inboundTopic, sampleElements))

      Await.result(control.drainAndShutdown(), 5.seconds) should be(Done)
      Await.result(control2.shutdown(), 5.seconds) should be(Done)
      result.futureValue should have size 3
    }

  }

  private def businessOperation(msg: String): Future[Unit] = Future.successful(())

}
