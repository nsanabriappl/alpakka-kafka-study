package com.example

import akka.{ Done, NotUsed }
import akka.kafka.ProducerMessage.Envelope
import akka.kafka.scaladsl.{ Consumer, Producer }
import akka.kafka.testkit.scaladsl.TestcontainersKafkaLike
import akka.kafka.{ ProducerMessage, ProducerSettings, Subscriptions }
import akka.stream.scaladsl.{ Keep, Sink, Source }
import akka.stream.testkit.scaladsl.TestSink
import org.apache.kafka.clients.producer.ProducerRecord

import scala.collection.immutable
import scala.concurrent.Future

class FlexiflowProducerSpec extends KafkaBaseSpec with TestcontainersKafkaLike {

  "Producer.flexiflow" must {

    "publish messages" in {
      val outboundTopic = createTopic(1)
      val groupId = createGroupId(1)

      val config = system.settings.config.getConfig("akka.kafka.producer")

      val producerSettings =
        ProducerSettings(config, StringSerializer, StringSerializer)
          .withBootstrapServers(bootstrapServers)

      val consumerSettings =
        consumerDefaults(StringDeserializer, StringDeserializer).withGroupId(groupId)

      val producer =
        Producer.flexiFlow[String, String, Envelope[String, String, NotUsed]](producerSettings)

      val sampleElements = List("Messi", "Ronaldo", "Pele")

      val published: Future[Done] =
        Source(sampleElements)
          .map { element =>
            val envelope: Envelope[String, String, Envelope[String, String, NotUsed]] =
              ProducerMessage.single(
                new ProducerRecord[String, String](outboundTopic, element + "-published2"),
                ProducerMessage.passThrough[String, String]
              )
            envelope
          }
          .via(producer)
          .map { p: ProducerMessage.Results[String, String, Envelope[String, String, NotUsed]] =>
            println("MESSAGE SENT to Kafka ===> " + p)
            p
          }
          .toMat(Sink.ignore)(Keep.right)
          .run()

      val probe = Consumer
        .plainSource(consumerSettings, Subscriptions.topics(outboundTopic))
        .map(_.value())
        .toMat(TestSink.probe)(Keep.right)
        .run()

      val consumed = probe
        .request(sampleElements.size)
        .expectNextN(sampleElements.size)

      consumed should contain theSameElementsInOrderAs sampleElements.map(_ + "-published2")

      probe.cancel()
      published.futureValue shouldBe Done
    }

    "publish messages with custom passThrough" in {
      val outboundTopic = createTopic(1)
      val groupId = createGroupId(1)

      val config = system.settings.config.getConfig("akka.kafka.producer")

      val producerSettings =
        ProducerSettings(config, StringSerializer, StringSerializer)
          .withBootstrapServers(bootstrapServers)

      val consumerSettings =
        consumerDefaults(StringDeserializer, StringDeserializer).withGroupId(groupId)

      val producer =
        Producer.flexiFlow[String, String, Envelope[String, String, Int]](producerSettings)

      val sampleElements = List("Messi", "Ronaldo", "Pele")

      val published: Future[Done] =
        Source(sampleElements)
          .map { element =>
            val goals = element.length
            val envelope: Envelope[String, String, Envelope[String, String, Int]] =
              ProducerMessage.single(
                new ProducerRecord[String, String](outboundTopic, element + "-published2"),
                ProducerMessage.passThrough[String, String, Int](goals)
              )
            envelope
          }
          .via(producer)
          .map { p =>
            println("MESSAGE SENT to Kafka ===> " + p)
            p
          }
          .mapAsync(1) {
            element: ProducerMessage.Results[String, String, Envelope[String, String, Int]] =>
              storeGoals(element.passThrough.passThrough)
          }
          .toMat(Sink.ignore)(Keep.right)
          .run()

      val probe = Consumer
        .plainSource(consumerSettings, Subscriptions.topics(outboundTopic))
        .map(_.value())
        .toMat(TestSink.probe)(Keep.right)
        .run()

      val consumed = probe
        .request(sampleElements.size)
        .expectNextN(sampleElements.size)

      consumed should contain theSameElementsInOrderAs sampleElements.map(_ + "-published2")

      probe.cancel()
      published.futureValue shouldBe Done
    }

    "publish messages conditionally" in {
      val outboundTopic = createTopic(1)
      val groupId = createGroupId(1)

      val config = system.settings.config.getConfig("akka.kafka.producer")

      val producerSettings =
        ProducerSettings(config, StringSerializer, StringSerializer)
          .withBootstrapServers(bootstrapServers)

      val consumerSettings =
        consumerDefaults(StringDeserializer, StringDeserializer).withGroupId(groupId)

      val sampleElements = List("Messi", "Ronaldo", "Pele")

      val published: Future[Done] =
        Source(sampleElements)
          .map { element =>
            //conditionally produce messages
            val envelope: Envelope[String, String, NotUsed] =
              if (element == "Messi")
                ProducerMessage.multi(
                  immutable.Seq(
                    new ProducerRecord[String, String](outboundTopic, element + "-published1"),
                    new ProducerRecord[String, String](outboundTopic, element + "-published2")
                  )
                )
              else if (element == "Pele")
                ProducerMessage.passThrough[String, String]()
              else
                ProducerMessage.single(
                  new ProducerRecord[String, String](outboundTopic, element + "-published3")
                )

            envelope
          }
          .via(Producer.flexiFlow(producerSettings))
          .map { p =>
            println("MESSAGE SENT to Kafka ===> " + p)
            p
          }
          .toMat(Sink.ignore)(Keep.right)
          .run()

      val probe = Consumer
        .plainSource(consumerSettings, Subscriptions.topics(outboundTopic))
        .map(_.value())
        .toMat(TestSink.probe)(Keep.right)
        .run()

      val consumed = probe
        .request(sampleElements.size)
        .expectNextN(sampleElements.size)

      consumed should contain theSameElementsInOrderAs Seq(
        "Messi-published1",
        "Messi-published2",
        "Ronaldo-published3"
      )

      probe.cancel()
      published.futureValue shouldBe Done
    }
  }

  private def storeGoals(n: Int): Future[Int] = Future.successful(n)

}
