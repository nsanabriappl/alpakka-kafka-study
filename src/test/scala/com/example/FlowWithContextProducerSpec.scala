package com.example

import akka.Done
import akka.kafka.scaladsl.Consumer.DrainingControl
import akka.kafka.scaladsl.{Committer, Consumer, Producer}
import akka.kafka.testkit.scaladsl.TestcontainersKafkaLike
import akka.kafka.{ProducerMessage, ProducerSettings, Subscriptions}
import akka.stream.scaladsl.{Keep, Sink, Source}
import akka.stream.testkit.scaladsl.TestSink
import org.apache.kafka.clients.producer.ProducerRecord

class FlowWithContextProducerSpec extends KafkaBaseSpec with TestcontainersKafkaLike {

  "Producer.flowWithContext" must {

    "publish messages" in {
      val outboundTopic = createTopic(1)
      val groupId = createGroupId(1)

      val config = system.settings.config.getConfig("akka.kafka.producer")

      val producerSettings =
        ProducerSettings(config, StringSerializer, StringSerializer)
          .withBootstrapServers(bootstrapServers)

      val consumerSettings =
        consumerDefaults(StringDeserializer, StringDeserializer).withGroupId(groupId)

      val sampleMessages = List("Messi", "Ronaldo", "Pele")

      val published = Source(sampleMessages)
         .asSourceWithContext(_.length)
        .map((message: String) =>
          ProducerMessage.single(
            new ProducerRecord[String, String](outboundTopic, message + "-published2")
          )
        )
        .via(Producer.flowWithContext(producerSettings))
        .map {p   =>
          println("MESSAGE SENT to Kafka ===> " + p)
          p
        }
        .runWith(Sink.ignore)

      val probe = Consumer
        .plainSource(consumerSettings, Subscriptions.topics(outboundTopic))
        .map(_.value())
        .toMat(TestSink.probe)(Keep.right)
        .run()

      val consumed = probe
        .request(sampleMessages.size)
        .expectNextN(sampleMessages.size)

      consumed should contain theSameElementsInOrderAs sampleMessages.map(_ + "-published2")

      probe.cancel()
      published.futureValue shouldBe Done
    }

    "consume and publish messages" in {
      val inboundTopic = createTopic(1)
      val outboundTopic = createTopic(1)
      val groupId = createGroupId(1)

      val config = system.settings.config.getConfig("akka.kafka.producer")

      val producerSettings =
        ProducerSettings(config, StringSerializer, StringSerializer)
          .withBootstrapServers(bootstrapServers)

      val committerSettings = committerDefaults

      val consumerSettings =
        consumerDefaults(StringDeserializer, StringDeserializer).withGroupId(groupId)

      val sampleMessages = List("Messi", "Ronaldo", "Pele")

      val published =
        Consumer
          .sourceWithOffsetContext(consumerSettings, Subscriptions.topics(inboundTopic))
          .map(message =>
            ProducerMessage.single(
              new ProducerRecord[String, String](outboundTopic, message + "-published2")
            )
          )
          .via(Producer.flowWithContext(producerSettings))
          .map { p =>
            println("MESSAGE SENT to Kafka ===> " + p)
            p
          }
          .toMat(Committer.sinkWithOffsetContext(committerSettings))(DrainingControl.apply)
          .run()

      awaitProduce(produceString(inboundTopic, sampleMessages))

      val probe = Consumer
        .plainSource(consumerSettings, Subscriptions.topics(outboundTopic))
        .map(_.value())
        .toMat(TestSink.probe)(Keep.right)
        .run()

      val consumed = probe
        .request(sampleMessages.size)
        .expectNextN(sampleMessages.size)

      consumed should contain theSameElementsInOrderAs sampleMessages.map(_ + "-published2")

      probe.cancel()
      //published.futureValue shouldBe Done
    }
  }

}
