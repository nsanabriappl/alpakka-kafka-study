package com.example

import akka.kafka.scaladsl.Consumer.DrainingControl
import akka.kafka.scaladsl.{Committer, Consumer, Producer}
import akka.kafka.testkit.scaladsl.TestcontainersKafkaLike
import akka.kafka.{ProducerMessage, Subscriptions}
import akka.stream.scaladsl.{Keep, Sink}
import akka.{Done, NotUsed}
import org.apache.kafka.clients.producer.ProducerRecord

import scala.concurrent.Await
import scala.concurrent.duration._

class SourceWithOffsetContextConsumerSpec extends KafkaBaseSpec with TestcontainersKafkaLike {

  "Consumer.sourceWithOffsetContext" must {

    "consume messages" in {
      val sampleElements = List("Messi", "Ronaldo", "Pele")
      val consumerSettings = consumerDefaults.withGroupId(createGroupId())
      val inboundTopic = createTopic(1)
      val outboundTopic = createTopic(2)

      val control =
        Consumer
          .sourceWithOffsetContext(consumerSettings, Subscriptions.topics(inboundTopic))
          .map { inboundMessage =>
            val envelope: ProducerMessage.Envelope[String, String, NotUsed] =  ProducerMessage.single(
              new ProducerRecord(outboundTopic, inboundMessage.key(), inboundMessage.value() + "-new-message" )
            )

            envelope
          }
          .via(Producer.flowWithContext(producerDefaults))
          .toMat(Committer.sinkWithOffsetContext(committerDefaults))(DrainingControl.apply)
          .run()

      val (control2, result) = Consumer
        .plainSource(consumerSettings, Subscriptions.topics(outboundTopic))
        .toMat(Sink.seq)(Keep.both)
        .run()

      awaitProduce(produceString(inboundTopic, sampleElements))


      Await.result(control.drainAndShutdown(), 5.seconds) should be(Done)
      Await.result(control2.shutdown(), 5.seconds) should be(Done)
      result.futureValue should have size 3
    }


  }

}
